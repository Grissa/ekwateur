package model;

public record ClientPro(String referenceClient, String siret, String raisonSociale, double CA) implements Client {
}
