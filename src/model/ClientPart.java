package model;

public record ClientPart(String referenceClient, String civilite, String nom, String prenom) implements Client {
}
