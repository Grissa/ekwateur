package service;

import model.Client;
import model.ClientPart;
import model.ClientPro;
import model.TypeEnergie;

public class FactureMensuelle {

    static final double SEUIL_CA = 1000000;
    static final double PRIX_ELECTRICITE_PARTICULIER = 0.133;
    static final double PRIX_GAZ_PARTICULIER = 0.108;
    static final double PRIX_ELECTRICITE_PRO_CA_SUPERIEUR = 0.110;
    static final double PRIX_GAZ_PRO_CA_SUPERIEUR = 0.123;
    static final double PRIX_ELECTRICITE_PRO_CA_INFERIEUR = 0.112;
    static final double PRIX_GAZ_PRO_CA_INFERIEUR = 0.117;
    private final Client client;
    private final double consommationMensuelleElec;
    private final double consommationMensuelleGaz;

    public FactureMensuelle(Client client, double consommationMensuelleElec, double consommationMensuelleGaz) {
        this.client = client;
        this.consommationMensuelleElec = consommationMensuelleElec;
        this.consommationMensuelleGaz = consommationMensuelleGaz;
    }

    private double calculerMontant(Client client, TypeEnergie typeEnergie, double consommationMensuelle) {
        double prixKWh = 0;

        switch (client) {
            case ClientPro clientPro -> {
                switch (typeEnergie) {
                    case ELECTRICITE -> prixKWh = clientPro.CA() > SEUIL_CA
                            ? PRIX_ELECTRICITE_PRO_CA_SUPERIEUR
                            : PRIX_ELECTRICITE_PRO_CA_INFERIEUR;
                    case GAZ -> prixKWh = clientPro.CA() > SEUIL_CA
                            ? PRIX_GAZ_PRO_CA_SUPERIEUR
                            : PRIX_GAZ_PRO_CA_INFERIEUR;
                }
            }
            case ClientPart ignored -> {
                switch (typeEnergie) {
                    case ELECTRICITE -> prixKWh = PRIX_ELECTRICITE_PARTICULIER;
                    case GAZ -> prixKWh = PRIX_GAZ_PARTICULIER;
                }
            }
        }
        return consommationMensuelle * prixKWh;
    }

    public double getMontantElec() {
        return calculerMontant(this.getClient(), TypeEnergie.ELECTRICITE, this.consommationMensuelleElec);
    }

    public double getMontantGaz() {
        return calculerMontant(this.getClient(), TypeEnergie.GAZ, this.consommationMensuelleGaz);
    }

    public double getMontantTotal() {
        return getMontantElec() + getMontantGaz();
    }

    public Client getClient() {
        return client;
    }
}
