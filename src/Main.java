import model.ClientPart;
import model.ClientPro;
import service.FactureMensuelle;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ClientPart clientParticulier = new ClientPart("EKW123456789", "Mme", "Dupont", "Alice");
        ClientPro clientProCaSup = new ClientPro("EKW987654321", "123456789", "Ma Société", 1200000);
        ClientPro clientProCaInf = new ClientPro("EKW147852369", "987654321", "Ma petite Société", 500000);

        try {
            System.out.print("Consommations mensuelles en kWh  pour l'électricité et le gaz du particulier (séparées par un espace) : ");
            var consoMensPartElec = scanner.nextDouble();
            var consoMensPartGaz = scanner.nextDouble();

            System.out.print("Consommations mensuelles en kWh  pour l'électricité et le gaz du client professionnelle avec un CA > 1M€ (séparées par un espace) : ");
            var consoMensProSupElec = scanner.nextDouble();
            var consoMensProSupGaz = scanner.nextDouble();

            System.out.print("Consommations mensuelles en kWh  pour l'électricité et le gaz du client professionnelle avec un CA < 1M€ (séparées par un espace) : ");
            var consoMensProInfElec = scanner.nextDouble();
            var consoMensProInfGaz = scanner.nextDouble();

            FactureMensuelle facturePart = new FactureMensuelle(clientParticulier, consoMensPartElec, consoMensPartGaz);
            FactureMensuelle factureProSup = new FactureMensuelle(clientProCaSup, consoMensProSupElec, consoMensProSupGaz);
            FactureMensuelle factureProInf = new FactureMensuelle(clientProCaInf, consoMensProInfElec, consoMensProInfGaz);

            List<FactureMensuelle> factures = new ArrayList<>();
            factures.add(facturePart);
            factures.add(factureProSup);
            factures.add(factureProInf);

            String[] options = {
                    "1- Montant Détaillé",
                    "2- Montant Total",
            };
            System.out.print("Quel type de montant voulez-vous ? : \n");
            for (String option : options) {
                System.out.println(option);
            }
            var option = scanner.nextInt();

            affichageResultat(option, factures);
        }
        catch (Exception e) {
            System.out.println("Veuillez n'entrer que deux chiffres (virgule possible) séparés par un espace");
        }
    }

    private static void affichageResultat(int option, List<FactureMensuelle> factures) {
        switch (option) {
            case 1 -> {
                for (FactureMensuelle facture : factures) {
                    if (facture.getClient() instanceof ClientPart) {
                        System.out.println(
                                "Montant à facturer pour " + ((ClientPart) facture.getClient()).civilite() +
                                        " " + ((ClientPart) facture.getClient()).nom() + " : \n" +
                                        facture.getMontantElec() + " € en électricité et " + facture.getMontantGaz() +
                                        " € en gaz.");
                    } else {
                        System.out.println(
                                "Montant à facturer pour la société " + ((ClientPro) facture.getClient()).raisonSociale()
                                        + " avec un CA de " + ((ClientPro) facture.getClient()).CA() + ": \n" +
                                        facture.getMontantElec() + " € en électricité et " + facture.getMontantGaz() +
                                        " € en gaz.");
                    }
                }
            }
            case 2 -> {
                for (FactureMensuelle facture : factures) {
                    if (facture.getClient() instanceof ClientPart) {
                        System.out.println(
                                "Montant à facturer pour " + ((ClientPart) facture.getClient()).civilite() +
                                        " " + ((ClientPart) facture.getClient()).nom() + " : \n" +
                                       facture.getMontantTotal() + " € au total.");
                    } else {
                        System.out.println(
                                "Montant à facturer pour la société " + ((ClientPro) facture.getClient()).raisonSociale()
                                        + " avec un CA de " + ((ClientPro) facture.getClient()).CA() + ": \n" +
                                        facture.getMontantTotal() + " € au total.");
                    }
                }
            }
        }
    }
}